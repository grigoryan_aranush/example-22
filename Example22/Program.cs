﻿using System;
class Example
{
    static void Main()
    {

        int iresult, irem;
        double dresult, drem;

        iresult = 10 / 3;
        irem = 10 % 3;

        dresult = 10.0 / 3.0;
        drem = 10.0 % 3.0;

        Console.WriteLine("Result and reminder of 10/3 is: " + iresult +" "+ irem);
        Console.WriteLine("Result and reminder of 10.0/3.0 is: " + dresult +" "+ drem);


    }
}